package enums;

public enum TypeOfArmor {
    CLOTH,
    LEATHER,
    MAIL,
    PLATE
}
