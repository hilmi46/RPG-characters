package items;

import enums.TypeOfArmor;
import attributes.MainAttributes;
import enums.Slot;

public class Armor extends Item{

    private final TypeOfArmor type;
    private final MainAttributes attribute;

    public Armor(String name, int minLevel, TypeOfArmor type, Slot slot , int power, int dexterity, int intelligence) {
        this.name = name;

        this.minLevel = minLevel;


        this.type = type;

        this.slot = slot;

        this.attribute = new MainAttributes(power, dexterity, intelligence);
    }

    public MainAttributes getAttribute() {
        return attribute;
    }

    public TypeOfArmor getType() {
        return type;
    }


}
