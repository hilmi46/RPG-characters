package items;

import enums.Slot;
import enums.TypeOfWeapon;

public class Weapon extends Item {
    private final TypeOfWeapon type;
    private final int damage;
    private final float attackSpeed;

    public Weapon(String name, int minLevel, TypeOfWeapon type, int damage, float attackSpeed) {
        this.minLevel = minLevel;
        this.name = name;
        this.attackSpeed = attackSpeed;
        this.type = type;
        this.damage = damage;
        this.slot = Slot.WEAPON;
    }

    public double getDps() {
        return this.getDamage() * this.getAttackSpeed();
    }
    public TypeOfWeapon getType() {
        return type;
    }

    public int getDamage() {
        return damage;
    }

    public float getAttackSpeed() {
        return attackSpeed;
    }
}
