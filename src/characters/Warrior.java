package characters;

import attributes.MainAttributes;
import enums.TypeOfArmor;
import enums.TypeOfWeapon;

public class Warrior extends Character {
    public Warrior(String name) {
        this.name = name;
        this.rootLevel = new MainAttributes(5, 2, 1);
        this.higherLevel = new MainAttributes(3, 2, 1);
        this.weaponList.add(TypeOfWeapon.AXE);
        this.weaponList.add(TypeOfWeapon.HAMMER);
        this.weaponList.add(TypeOfWeapon.SWORD);
        this.armorList.add(TypeOfArmor.MAIL);
        this.armorList.add(TypeOfArmor.PLATE);
    }

    @Override
    public double getDps() {
        return 0;
    }
}
