package characters;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CharacterTest {
    @Test
    public void Test_LevelingUp_MakesLevel2() {
        Mage character = new Mage("Name");
        character.levelUp();
        assertEquals(2, character.getLevel());
    }

}