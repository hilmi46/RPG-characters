package characters;


import java.util.ArrayList;

import java.util.HashMap;

import java.util.Map;

import attributes.MainAttributes;

import enums.Slot;
import enums.TypeOfArmor;
import exceptions.InvalidArmorException;
import exceptions.InvalidWeaponException;

import enums.TypeOfWeapon;

import items.Weapon;
import items.Item;
import items.Armor;

// Character specifications
public abstract class Character {
    // Value of name
    protected String name;

    public int getLevel() {
        return level;
    }

    // Value of root Level
    protected int level = 1;

    // Attributes for each character is shared.
    protected MainAttributes baseAttributes;

    protected MainAttributes higherLevel;


    protected HashMap<Slot, Item> equipment = new HashMap<>();
    protected ArrayList<TypeOfWeapon> weaponList = new ArrayList<>();
    protected ArrayList<TypeOfArmor> armorList = new ArrayList<>();

    public void levelUp() {
        this.level = level + 1;

        baseAttributes.setPower( baseAttributes.getPower() + higherLevel.getPower());
        baseAttributes.setDexterity( baseAttributes.getDexterity() + higherLevel.getDexterity());
        baseAttributes.setIntelligence( baseAttributes.getIntelligence() + higherLevel.getIntelligence());
        ;



    } //// AAN DE SLAG Intelligence,
    public boolean equipItem(Item item) {
        if (item.getSlot() == Slot.WEAPON) {
            if (!weaponList.contains(((Weapon) item).getType()))
                throw new InvalidWeaponException("Cannot equip weapon type " + ((Weapon) item).getType());
            if (item.getMinLevel() > level)
                throw new InvalidWeaponException("Weapon level too high, cannot be equipped.");
        }
        else {
            if (!armorList.contains(((Armor) item).getType()))
                throw new InvalidArmorException("Cannot equip armor type " + ((Armor) item).getType());
            if (item.getMinLevel() > level)
                throw new InvalidArmorException("Armor level too high, cannot be equipped.");
        }
        equipment.put(item.getSlot(), item);
        return true;
    }
    private MainAttributes getArmorAttributes() {
        int power = 0;
        int dexterity = 0;
        int intelligence = 0;
        for (Map.Entry<Slot, Item> entry : equipment.entrySet()) {
            if (entry.getKey() != Slot.WEAPON) {
                power += ((Armor) entry.getValue()).getAttribute().getPower();
                dexterity += ((Armor) entry.getValue()).getAttribute().getDexterity();
                intelligence += ((Armor) entry.getValue()).getAttribute().getIntelligence();
            }
        }
        return new MainAttributes(power, dexterity, intelligence);
    }
    public abstract double getDps();

}