package characters;

import attributes.MainAttributes;
import enums.TypeOfArmor;
import enums.TypeOfWeapon;

public class Ranger extends Character {

    public Ranger(String name) {

        this.name = name;

        this.rootLevel = new MainAttributes(1, 7, 1);

        this.higherLevel = new MainAttributes(1, 5, 1);

        this.weaponList.add(TypeOfWeapon.BOW);

        this.armorList.add(TypeOfArmor.MAIL);

        this.armorList.add(TypeOfArmor.LEATHER);

    }

    @Override
    public double getDps() {
        return 0;
    }
}