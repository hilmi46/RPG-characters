package characters;

import attributes.MainAttributes;
import enums.TypeOfArmor;
import enums.TypeOfWeapon;


public class Mage extends Character {


    public Mage(String name) {
        this.name = name;

        this.rootLevel = new MainAttributes(1, 1, 8);

        this.higherLevel = new MainAttributes(1, 1, 5);

        this.weaponList.add(TypeOfWeapon.WAND);

        this.armorList.add(TypeOfArmor.CLOTH);

        this.weaponList.add(TypeOfWeapon.STAFF);
    }

    @Override
    public double getDps() {
        double weaponDPS = 1;
        // Get dps from Weapon
        // Character DPS = Weapon DPS * (1 + TotalIntelligenceAttribute/100)
        return 0;
    }

}