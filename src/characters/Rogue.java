package characters;

import attributes.MainAttributes;
import enums.TypeOfArmor;
import enums.TypeOfWeapon;

public class Rogue extends Character {
    public Rogue(String name) {
        this.name = name;
        this.rootLevel = new MainAttributes(2, 6, 1);
        this.higherLevel = new MainAttributes(1, 4, 1);
        this.weaponList.add(TypeOfWeapon.DAGGER);
        this.weaponList.add(TypeOfWeapon.SWORD);
        this.armorList.add(TypeOfArmor.LEATHER);
        this.armorList.add(TypeOfArmor.MAIL);
    }


    @Override
    public double getDps() {
        return 0;
    }
}
