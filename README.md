# RPG Characters


The goal of this assignment was to get used to OOP by creating fictional characters which had different attributes such as: Strength, dexterity and intelligence. 

There are also items such as weapons and clothing. Certain items can only be worn if conditions are met (such as level and type of character).

This project will also contain testing which is on its way.

## Table of Contents

- [Maintainers](#maintainers)
- [Contributing](#contributing)
- [License](#license)

## Maintainers

[@hilmi46 (Ahmet Hilmi Terzi)](https://github.com/@hilmi46)

## Contributing

[@hilmi46 (Ahmet Hilmi Terzi)](https://github.com/@hilmi46)

## License

MIT © 2022 Ahmet Hilmi Terzi
